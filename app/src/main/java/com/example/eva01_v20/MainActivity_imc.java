package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Usuario;

import java.text.DecimalFormat;

public class MainActivity_imc extends AppCompatActivity {

    TextView imc;
    EditText pesoActual;
    Usuario usuario;
    double estatura;
    double peso;
    double imcCalculado;
    String correo;
    @Override

    //faltaba findviewIMC
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_imc);

        imc = findViewById(R.id.txtimc);
        pesoActual = findViewById(R.id.txtimcweight);
        correo = getIntent().getExtras().getString("correo");
        obtenerUsuario(correo);

    }
    private void obtenerUsuario(String correo) {
        UsuarioDao usuarioDao = new UsuarioDao(getApplicationContext());
        usuario = usuarioDao.getUsuarioPorEmail(getApplicationContext(), correo);

        if (!(usuario == null)) {
            estatura = usuario.getEstatura();
        } else {
            Toast.makeText(this, "Instancia de usuario no existe", Toast.LENGTH_SHORT).show();
        }
    }
    public void calcularImc(View view) {
        estatura = usuario.getEstatura();
        peso = Double.parseDouble(pesoActual.getText().toString().trim());
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        imcCalculado = (peso/Math.pow(estatura,2));
        String imcFinal = decimalFormat.format(imcCalculado);
        imc.setText(imcFinal);
    }

    //Método para ir a Crear Registro
    public void Crear (View view){
        Intent crear = new Intent(this, MainActivity_RegisterEV.class);
        crear.putExtra("peso","");
        crear.putExtra("imc","");
        crear.putExtra("correo",correo);
        startActivity(crear);
    }

}